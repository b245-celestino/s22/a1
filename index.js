let registeredUsers = [
  "James Jeffries",
  "Gunther Smith",
  "Macie West",
  "Michelle Queen",
  "Shane Miguelito",
  "Fernando Dela Cruz",
  "Akiko Yukihime",
];

let friendsList = [];

function addNewUser(add) {
  let check = registeredUsers.includes(add);
  // console.log(check);
  if (check === false) {
    registeredUsers.push(add);
    alert("Thank you for registering!");
  } else if (check == true) {
    alert("Registration failed. Username already exists!");
  }
}

addNewUser("Anna");
console.log(registeredUsers);

function addFriend(foundUser) {
  let check = registeredUsers.includes(foundUser);
  // console.log(check);
  if (check === false) {
    alert("user not found");
  } else {
    friendsList.push(foundUser);
    alert("You have added " + foundUser + "as a friend");
  }
}
addFriend("Akiko Yukihime");
console.log(friendsList);

function showFriends() {
  if (friendsList.length == 0) {
    alert(
      "You currently have " + friendsList.length + " friends. Add one first."
    );
  } else {
    function show(friendsList) {
      return friendsList;
    }

    let friends = friendsList.map(show);
    console.log(friends);
  }
}

showFriends();
console.log(friendsList);

function displayUser() {
  if (friendsList.length == 0) {
    alert(
      "You currently have " + friendsList.length + " friends. Add one first."
    );
  } else {
    alert("You currently have " + friendsList.length + " friends.");
  }
}

displayUser();

function deleteUser() {
  if (friendsList.length == 0) {
    alert(
      "You currently have " + friendsList.length + " friends. Add one first."
    );
  } else {
    friendsList.pop();
  }
}

deleteUser();
console.log(friendsList);

// Stretch goal

function deleteSpecific(user) {
  let deletedUser = registeredUsers.indexOf(user);
  newArr = registeredUsers.splice(deletedUser, 1);
  // console.log(newArr);
  console.log(registeredUsers);
}

deleteSpecific("Macie West");
